<?php 

/*
Template Name: Special Layout
*/

get_header();

if (have_posts()) :
	while (have_posts()) : the_post(); ?>

	<article class="post page">

		<h2><?php the_title(); ?></h2>

		<!-- info-box -->
		<div class="info-box">
			<h4>Disclaimer for Paradise Resort</h4>
			<p>If you require any more information or have any questions about our site’s disclaimer, please feel free to contact us by email at paradiseresort@gmail.com. <br><br>

All the information on this website is published in good faith and for general information purpose only. Paradise Resort does not make any warranties about the completeness, reliability and accuracy of this information. Any action you take upon the information you find on this website (Paradise Resort), is strictly at your own risk. Paradise Resort will not be liable for any losses and/or damages in connection with the use of our website.



Consent
By using our website, you hereby consent to our disclaimer and agree to its terms.
</p> 
		</div><!-- /info-box -->

		<?php the_content(); ?>

	</article>

		<?php endwhile;

		else :
			echo '<p>No content found</p>';
		endif;

	get_footer();

	?>




